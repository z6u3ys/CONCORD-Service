//
//  ProjectVC.m
//  CONCORD-Service
//
//  Created by Sheng's on 2021/6/24.
//

#import "ProjectVC.h"
#import "ProjectCell.h"

@interface ProjectVC ()<UITableViewDelegate, UITableViewDataSource>

@end

@implementation ProjectVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.mtableView.delegate = self;
    self.mtableView.dataSource = self;
    [self.mtableView registerNib:[UINib nibWithNibName:@"ProjectCell" bundle:nil] forCellReuseIdentifier:@"mCell"];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ProjectCell *cell = [tableView dequeueReusableCellWithIdentifier:@"mCell" forIndexPath:indexPath];
    cell.timeLineSTV.hidden = YES;
    cell.title.text = @"aa";
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    ProjectCell *cell = [tableView dequeueReusableCellWithIdentifier:@"mCell" forIndexPath:indexPath];
    if (cell.timeLineSTV.hidden) {
        cell.timeLineSTV.hidden = NO;
    }else {
        cell.timeLineSTV.hidden = YES;
    }
    NSLog(@"%@",cell.timeLineSTV.hidden);
    [self.mtableView reloadData];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
