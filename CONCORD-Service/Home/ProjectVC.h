//
//  ProjectVC.h
//  CONCORD-Service
//
//  Created by Sheng's on 2021/6/24.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProjectVC : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *mtableView;

@property (assign) NSInteger expandedSectionHeaderNumber;
@property (assign) UITableViewHeaderFooterView *expandedSectionHeader;
@property (strong) NSArray *sectionItems;
@property (strong) NSArray *sectionNames;

@end

NS_ASSUME_NONNULL_END
