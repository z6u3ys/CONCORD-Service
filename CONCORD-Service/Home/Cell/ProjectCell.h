//
//  ProjectCell.h
//  CONCORD-Service
//
//  Created by Sheng's on 2021/6/24.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProjectCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UIStackView *timeLineSTV;

@end

NS_ASSUME_NONNULL_END
