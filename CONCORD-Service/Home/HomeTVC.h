//
//  HomeTVC.h
//  CONCORD-Service
//
//  Created by Sheng's on 2021/6/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeTVC : UITableViewController
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *collectionViewFlowLayout;
- (IBAction)mainBTN_1:(id)sender;


@end

NS_ASSUME_NONNULL_END
