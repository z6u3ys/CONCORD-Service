//
//  LoginVC.h
//  CONCORD-Service
//
//  Created by Sheng's on 2021/6/17.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LoginVC : UIViewController
- (IBAction)loginAction:(id)sender;

@end

NS_ASSUME_NONNULL_END
